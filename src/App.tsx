import { useState } from "react";
import "./App.css";
import hide from "./icon/jpg/hide.jpg";
import flag from "./icon/jpg/flag.jpg";
import interrogate from "./icon/jpg/interrogate.jpg";
import boom from "./icon/jpg/mine_boom.jpg";
import mine from "./icon/jpg/mine.jpg";

function App() {
  const [boxBoard, setBoxBoard] = useState(9);
  const [box, setBox]: any = useState({
    isOpen: false,
    isMine: false,
    isFlag: false,
    isHide: true,
  });
  const [mineNumber, setMineNumber] = useState(10);
  // const [board, setBoard]: any = useState([]);
  const niveau = ["Débutant", "Confirmé", "Expert"];

  // construit un tableau avec box commençant par 1 au lieu de 0
  const caseNumber = Array.from(Array(boxBoard), (_, i) => i + 1);
  for (let i = 0; i < boxBoard; i++) {
    caseNumber.splice(i, 1, box);
  }

  //création du tableau
  const tableau = [...caseNumber].map((i, col) => {
    return (
      <tr key={col + "c"}>
        {[...caseNumber].map((i, rows) => {
          return (
            <td
              key={rows + "r"}
              className="case"
              data-value={i}
              onMouseUp={(e) => handleClick(col, rows, e, i)}
            >
              <img className="icon" src={hide} alt="hide" />
            </td>
          );
        })}
      </tr>
    );
  });

  // for (let i = 0; i < boxBoard * boxBoard - mineNumber; i++) {
  //   board.push(box);
  // }
  // for (let i = 0; i < mineNumber; i++) {
  //   board.push("mine");
  // }
  // const randomBoard = board.sort(() => Math.random() - 0.5);
  // console.log(randomBoard);

  const handleLevel = (index: any) => {
    switch (index) {
      case "Confirmé":
        setBoxBoard(16);
        setMineNumber(40);
        break;
      case "Expert":
        setBoxBoard(32);
        setMineNumber(99);
        break;
      default:
        setBoxBoard(9);
        setMineNumber(10);
    }
    return;
  };

  // const handleMouseClick = function (e: any) {
  //   if (e.nativeEvent.which === 1) {
  //     console.log("Left");
  //   } else if (e.nativeEvent.which === 3) {
  //     console.log("Right");
  //   }
  // };

  const handleClick = function (col: any, rows: any, e: any, value: any) {
    console.log(col - 1, rows - 1);
    console.log(col - 1, rows);
    console.log(col - 1, rows + 1);
    console.log(col, rows - 1);
    console.log("click", col, rows);
    console.log(col, rows + 1);
    console.log(col + 1, rows - 1);
    console.log(col + 1, rows);
    console.log(col + 1, rows + 1);
    // console.log(value.isHide);
    
    console.log(box);
    
    if (e.nativeEvent.which === 1) {
      console.log("Left");
    } else if (e.nativeEvent.which === 3) {
      console.log("Right");
    }
    if (value.isHide === true) {
      console.log("test");
      setBox([...box, {isHide: false}]);
      console.log(value);
    } else {
      console.log("test32");
      console.log(value);
    }
  };

  return (
    <div className="App">
      {/* <button className="btn btn-secondary" onMouseDown={handleMouseClick}>
        Click mouse
      </button> */}
      <h1>Démineur</h1>
      {[...niveau].map((index) => {
        return (
          <button
            className="button"
            key={index}
            onClick={() => handleLevel(index)}
          >
            {index === "Débutant"
              ? niveau[0]
              : index === "Confirmé"
              ? niveau[1]
              : "Expert"}
          </button>
        );
      })}
      <table className="table">
        <thead className="allHead">
          <tr className="head">Mines : {mineNumber}</tr>
        </thead>
        <tbody className="tableau">
          {[...caseNumber].map((i, col) => {
            return (
              <tr key={col + "c"}>
                {[...caseNumber].map((i, rows) => {
                  return (
                    <td
                      key={rows + "r"}
                      className="case"
                      data-value={i}
                      onMouseUp={(e) => handleClick(col, rows, e, i)}
                    >
                      <img className="icon" src={hide} alt="hide" />
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
